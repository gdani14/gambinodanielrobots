<?php

namespace App\Controller;

use App\Entity\Robot;
use App\Form\RobotType;
use App\Repository\RobotRepository;
use http\Message\Body;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class RobotController extends AbstractController
{
    #[Route('/', name: 'app_robot_index', methods: ['GET'])]
    public function index(RobotRepository $robotRepository): Response
    {
        return $this->render('robot/index.html.twig', [
            'robots' => $robotRepository->findBy(['deleted' => false]),
        ]);
    }

    #[Route('/new', name: 'app_robot_new', methods: ['GET', 'POST'])]
    public function new(Request $request, RobotRepository $robotRepository): Response
    {
        $robot = new Robot();
        $form = $this->createForm(RobotType::class, $robot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $robotRepository->save($robot, true);

            return $this->redirectToRoute('app_robot_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('robot/new.html.twig', [
            'robot' => $robot,
            'form' => $form,
        ]);
    }

    #[Route('/show/{id}', name: 'app_robot_show', methods: ['GET'])]
    public function show(Robot $robot): Response
    {
        return $this->render('robot/show.html.twig', [
            'robot' => $robot,
        ]);
    }

    #[Route('/edit/{id}', name: 'app_robot_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Robot $robot, RobotRepository $robotRepository): Response
    {
        $form = $this->createForm(RobotType::class, $robot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $robotRepository->save($robot, true);

            return $this->redirectToRoute('app_robot_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('robot/edit.html.twig', [
            'robot' => $robot,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: 'app_robot_delete', methods: ['POST'])]
    public function delete(Request $request, Robot $robot, RobotRepository $robotRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $robot->getId(), $request->request->get('_token'))) {
            //$robotRepository->remove($robot, true);
            $robot->setDeleted(true);
            $robotRepository->save($robot, true);
        }

        return $this->redirectToRoute('app_robot_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/fight', name: 'app_robot_fight', methods: ['GET'])]
    public function fight(Request $request, RobotRepository $robotRepository): Response
    {
        $content = $request->query->all('selectedRobots');

        return $this->render('robot/fight.html.twig', [
            'robot' => $this->getStrongestRobot($robotRepository, $content)
        ]);
    }

    #[Route('/fight_api', name: 'app_robot_fight_api', methods: ['GET'])]
    public function fightAPI(Request $request, RobotRepository $robotRepository): Response
    {
        $content = json_decode($request->getContent(), true);

        $strongestRobot = null;
        if (is_array($content)) {
            $strongestRobot = $this->getStrongestRobot($robotRepository, $content);
        }

        return $this->json($strongestRobot);
    }

    /**
     * @param RobotRepository $robotRepository
     * @param array $content
     * @return void
     */
    public function getStrongestRobot(RobotRepository $robotRepository, array $content): Robot
    {
        return $robotRepository->findOneBy(array('id' => $content), array('power' => 'DESC', 'createdAt' => 'DESC'), 1);
    }

}

