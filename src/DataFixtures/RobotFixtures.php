<?php

namespace App\DataFixtures;

use App\Entity\Robot;
use App\Entity\RobotType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class RobotFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $robotTypes = $manager->getRepository(RobotType::class)->findAll();

        for ($i = 0; $i < 50; $i++) {
            $robot = new Robot();
            $robot->setName($faker->name);
            $robot->setType($faker->randomElement($robotTypes));
            $robot->setPower($faker->randomNumber());
            $robot->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime));
            $manager->persist($robot);
        }


        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [RoboTypeFixtures::class];
    }
}
