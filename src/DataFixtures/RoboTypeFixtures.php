<?php

namespace App\DataFixtures;

use App\Entity\RobotType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoboTypeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $robotType = new RobotType('brawler');
        $manager->persist($robotType);
        $robotType = new RobotType('rouge');
        $manager->persist($robotType);
        $robotType = new RobotType('assault');
        $manager->persist($robotType);

        $manager->flush();
    }
}
