1. Create MySQL database (latest version) and put connection data to the .env file!
2. Migrate with: php bin/console doctrine:migrations:migrate
3. Generate random data with: php bin/console doctrine:fixtures:load
4. Start local server with: symfony local:server:start
